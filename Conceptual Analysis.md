Block chain over VoIP

In this project, we will disuss about possible application of block chain with VoIP communication.

Block chain is a truly decentralised digial ledger in which transactions are made.One advantage of block chain is data integrity and decentralised authentication. Both of which can be applied to exisiting VoIP technologies.
One popular possibility could be to use block chain and Smart contracts between different SIP server/nodes in a scalable/secure and decentralised way.

In our project we will demonstrate multiple nodes/server communicating with each other and authenticating via Blockchain and sharing their address books in one encrypted ledger. We have assumed 3 SIP servers with their own multiple users in their particualr network. These SIP servers are connected via Internet. Each server maintains its own address book of users registered to that server. The user of one SIP server can not talk to users of other SIP servers without any sort of contract between these two servers.This is where block chain comes in use. 

Block chain allows smart contracts between two parties on a particular transaction.In our case smart contract will include sharing of address book ,with each transaction being a voice call between users in that address book. 
Each block in a block chain will contain collection of address books of SIP servers and transactions which will keep updating , hence generating a new block.

Understanding the concept of SIP calling and Blockchain: 
Every server maintain a local address book. One server can not call to other server until it is registered to that server. This registration is not possible unless copy of local address books of all SIP servers are contained in a Ledger (block)  and this ledger is shared with every SIP server.
Block chain is maintained and updated. Same copy of ledger will allow one SIP server to register to another SIP server hence allowing the users of both servers or networks  to communicate with each other.

Advantage:
1. Small contracts between servers can be maintained to limit/allow certain amount of calls to or from them with certain amount of billing.
2. Adding new servers/ removing servers/ updating servers(removing / adding a new user) new block will be formed.
3. Privacy (If one wants to communicate just within his/her network his address will not be maintained in the ledger)
4. Decentralised ledger, no one is specifically maintaining it.
5. Scalable.( new nodes can be added/removed)

